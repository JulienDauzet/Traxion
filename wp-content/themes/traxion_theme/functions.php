<?php
/**
** activation theme
**/
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles', 11 );
//var_dump(get_template_directory_uri());
//die();
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_uri() );


    wp_enqueue_script('traxion-js',
        '/wp-content/themes/traxion_theme/js/traxion.js',
        [],
        '',
        true);

    wp_enqueue_script('traxion-calculs-js',
        '/wp-content/themes/traxion_theme/js/calculs.js',
        [],
        '',
        true);
}

add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size ) {
    return array(
        'width' => 150,
        'height' => 100,
        'crop' => 0,
    );
} );