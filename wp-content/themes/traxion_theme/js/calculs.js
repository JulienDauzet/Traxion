/* Hérité de la de la v1 de Traxion
 Je ne suis pas responsable de ce code */

/* Porteurs et remorques à essieux centraux */


jQuery(function ($) {


    $("#calc-DC").click(function () {


        function virgule(texte) {
            while (texte.indexOf(',') > -1) {
                texte = texte.replace(",", ".");
            }

            return texte
        }


        var val_T = virgule($('#b12').val());
        var val_R = virgule($('#b13').val());
        var val_S = virgule($('#b14').val());
        var val_S_calc = val_S * 0.001;

        var gravité = '9.81';
        var val_C = val_R - val_S_calc;

        var val_T_calc = parseFloat(val_T) + parseFloat(val_S_calc);

        var calcul_1 = val_T_calc * val_C;
        var calcul_2 = parseFloat(val_T_calc) + parseFloat(val_C);
        var calcul_3 = calcul_1 / calcul_2;

        var resultat = gravité * calcul_3;

        $('#b15').val(resultat.toFixed(2));

    });

    $("#calc-V").click(function () {   /*fonction pour detecter le click */
        var radio_val = $('input[type=radio][name=type_suspension]:checked').attr('value');


        function virgule(texte) {
            while (texte.indexOf(',') > -1) {
                texte = texte.replace(",", ".");
            }

            return texte
        }


        if (radio_val == 'pneu') {

            var val_X = virgule($('#b23').val());
            var val_L = virgule($('#b24').val());
            var val_R2 = virgule($('#b25').val());
            var val_S2 = virgule($('#b26').val());

            val_X = val_X * 0.001;
            val_L = val_L * 0.001;

            var val_S2_calc = val_S2 * 0.001;
            var val_C2 = val_R2 - val_S2_calc;

            var pneu_val = 1.8;
            var calcul_1 = val_X * val_X;
            var calcul_2 = val_L * val_L;
            var calcul_3 = calcul_1 / calcul_2;
            var calcul_4 = calcul_3 * val_C2;

            var resultat2 = pneu_val * calcul_4;

            $('#b28').val(resultat2.toFixed(2));


        } else {

            var val_X = virgule($('#b23').val());
            var val_L = virgule($('#b24').val());
            var val_R2 = virgule($('#b25').val());
            var val_S2 = virgule($('#b26').val());

            val_X = val_X * 0.001;
            val_L = val_L * 0.001;


            var val_S2_calc = val_S2 * 0.001;
            var val_C2 = val_R2 - val_S2_calc;

            var meca_val = 2.4;
            var calcul_1 = val_X * val_X;
            var calcul_2 = val_L * val_L;
            var calcul_3 = calcul_1 / calcul_2;
            var calcul_4 = calcul_3 * val_C2;

            var resultat2 = meca_val * calcul_4;

            $('#b28').val(resultat2.toFixed(2));


        }    });
});

/* Porteurs et remorques à rond d’avant trains */


jQuery(function ($) {

    $("#calc-steering").click(function () {

        function virgule(texte) {
            while (texte.indexOf(',') > -1) {
                texte = texte.replace(",", ".");
            }

            return texte
        }

        var val_T = virgule($('#b4').val());
        var val_R = virgule($('#b5').val());
        var gravité = '9.81';

        var calcul_1 = val_T * val_R;
        var calcul_2 = gravité * calcul_1;
        var calcul_3 = parseFloat(val_T) + parseFloat(val_R);
        var resultat = calcul_2 / calcul_3;

        $('#b7').val(resultat.toFixed(2));

    });


});

// Tracteurs et semi-remorques


jQuery(function ($) {


    $("#calc-semi").click(function () {


        function virgule(texte) {
            while (texte.indexOf(',') > -1) {
                texte = texte.replace(",", ".");
            }

            return texte
        }


        var val_T = virgule($('#b34').val());
        var val_R = virgule($('#b35').val());
        var val_U = virgule($('#b36').val());
        var gravité = '9.81';
        var val_T_calc = parseFloat(val_T) + parseFloat(val_R) - parseFloat(val_U);
        var calcul_1 = 0.6 * parseFloat(val_T) * val_R;
        var calcul_2 = calcul_1 / val_T_calc;
        var resultat = gravité * calcul_2;

        $('#DSemi').val(resultat.toFixed(2));

    });


});
