jQuery(window).on("load", function () {
    paddingContainer();

    window.addEventListener("resize", paddingContainer);

    function paddingContainer() {
        const containerWidth = jQuery('header .elementor-container').width();
        const windowWidth = jQuery(window).width();
        const paddingAdjust = (windowWidth - containerWidth) / 2;

        jQuery('.paddingContainerLeft .elementor-element-populated').each(function () {
            if (jQuery(window).width() >= 767) {
                jQuery(this).css('padding-left', paddingAdjust + 'px');
            } else {
                jQuery(this).css('padding-left', '20px');

            }
        });
        jQuery('.paddingContainerRight .elementor-element-populated').each(function () {
            if (jQuery(window).width() >= 767) {
                jQuery(this).css('padding-right', paddingAdjust + 'px');
            } else {
                jQuery(this).css('padding-right', '20px');

            }
        });
    }


});

