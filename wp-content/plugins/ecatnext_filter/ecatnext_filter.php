<?php
/*
* Plugin Name: EcatNext create filter
* Plugin URI:
* Description: Create filter after import from NextPage.
* Version: 0.1
* Slug: ecatnext_filter
* Author: 3C-evolution Jean-Noel Colliard
* Author URI: https://www.3c-e.com
* License: GPLv2 or later
* License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/
?>
<?php

require_once(__DIR__ . '/../../../wp-config.php');
class importFilter
{
    
    function __construct()
    {
        if (isset($_GET["ecatnext_import_filter"])) :
            $results = $this->_start_import();
            $date = new DateTime();
            echo '<pre>' . print_r($date, true) . '</pre>' . "\n" . '<pre>' . print_r($results, true) . '</pre>';
        endif;
    }
    private function _start_import()
    {
        set_time_limit(1200);
        $results = [];
        $results = $this->_import_filter();
        return $results;
    }
    private function _import_filter()
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        if ($mysqli->connect_error) :
            die($mysqli->connect_errno);
        endif;

        global $wpdb;

        $requestSelect = "SELECT * FROM " . $wpdb->prefix . "woocommerce_attribute_taxonomies";
        $resultSelect = $mysqli->query($requestSelect);
        $row = $resultSelect->num_rows;
        $filter = [];
        $varWidget = ['dgwt_wcas_ajax_search-11', 'woocommerce_product_categories-2', 'woocommerce_layered_nav_filters-2'];
        if ($row > 0) :
            $i = 0;
            foreach ($resultSelect as $result) :
                $requestDico = "SELECT * FROM " . $wpdb->prefix . "ecatnext_dico AS Dico," . $wpdb->prefix . "ecatnext_type AS Type WHERE Dico.dico_label='" . utf8_encode($result['attribute_label']) . "' AND Dico.dico_extid=Type.type_value";
                $resultDico = $mysqli->query($requestDico);
                $requestCount = mysqli_num_rows($resultDico);
                if ($requestCount > 0) :
                    $filter[$i] = array(
                        'title' => 'Filteren op ' . $result['attribute_label'],
                        'attribute' => $result['attribute_name'],
                        'display_type' => 'list',
                        'query_type' => 'and'
                    );
                    $varWidget[] = 'woocommerce_layered_nav-' . $i;
                    $i++;
                endif;
            endforeach;
        endif;
        $mysqli->close();
        $filter[] = ['multiwidget' => 1];
        update_option('widget_woocommerce_layered_nav', $filter);
        $widget = [
            'astra-woo-shop-sidebar' => $varWidget,
        ];
        update_option('sidebars_widgets', $widget);
        
        $this->_import_orders();
        return $varWidget;
    }
    private function _import_orders()
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        if ($mysqli->connect_error) :
            die($mysqli->connect_errno);
        endif;
        $requestOrders = "SELECT * FROM " . $wpdb->prefix . "ecatnext_canal WHERE canal_productid=0";
        $resultOrders = $mysqli->query($requestOrders);
        foreach ($resultOrders as $orders) :
            $verifOrders = $orders['canal_label'];
            $verifCanalOrders = $orders['canal_order'];
            $requestTerms = "SELECT * FROM " . $wpdb->prefix . "terms WHERE name='" . $verifOrders . "'";
            $resultTerms = $mysqli->query($requestTerms);
            foreach ($resultTerms as $terms) :
                $verifReturn = $terms['term_id'];
                $requestReturn = "SELECT * FROM " . $wpdb->prefix . "termmeta WHERE (meta_key='product_count_product_cat' AND meta_value>0) AND term_id='$verifReturn'";
                $resultReturn = $mysqli->query($requestReturn);
                $requestCount = mysqli_num_rows($resultReturn);
                if ($requestCount > 0) :
                    $requestOrders = "UPDATE " . $wpdb->prefix . "termmeta SET meta_value='$verifCanalOrders' WHERE meta_key='order' AND term_id='$verifReturn'";
                    $mysqli->query($requestOrders);
                endif;
            endforeach;
        endforeach;
        $mysqli->close();
    }
}
new importFilter();
?>