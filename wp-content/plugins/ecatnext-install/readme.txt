=== EcatNext install ===
Contributors: 3C-evolution Jean-Noel Colliard ]||[
Tags: ecatnext install,ecatnext canal,ecatnext dico,ecatnext type,ecatnext product
Author URI: https://www.3c-e.com
Plugin URI: https://www.3c-e.com
Requires at least: 5.5.1
Tested up to: 5.5.1
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Created new tables for helping import from NextPage. Help import for Canal, Dico, Type, Product and Filter from NextPage.

== Description ==
This plugin was developped for create a synchronization between your WordPress Woocommerce and your PIM NextPage.

== Installation ==
1. Upload folder "ecatnext-install" to the "plugins" directory.
2. Activate the plugin through the "Plugins" menu in WordPress.

== Changelog ==
= 1.0 =
Initial release.

== Upgrade Notice ==
= 1.0 =
First version after developping