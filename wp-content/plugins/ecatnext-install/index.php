<?
//Theory is when we know everything but nothing works.
//Praxis is when everything works but we do not know why.
//We always end up by combining theory with praxis: nothing works and we do not know why.