<?php
/*
* Plugin Name: EcatNext install
* Plugin URI: https://www.3c-e.com
* Description: Created new tables for helping import from NextPage. Help import for Canal, Dico, Type, Product and Filter from NextPage.
* Version: 0.1
* Slug: ecatnext-install
* Author: 3C-evolution Jean-Noel Colliard ]||[
* Author URI: https://www.3c-e.com
* License: GPLv2 or later
* License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/
?>
<?php
/*
* ------------------------------------------------- *
* ]||[ Load and include files
* ------------------------------------------------- *
*/
require_once('inc/includes.php');
/*
* ------------------------------------------------- *
* ]||[ Plugin activate/deactivate
* ------------------------------------------------- *
*/
register_activation_hook(__FILE__, array('ecatnextRegister', 'install'));
register_deactivation_hook(__FILE__, array('ecatnextRegister', 'uninstall'));
?>