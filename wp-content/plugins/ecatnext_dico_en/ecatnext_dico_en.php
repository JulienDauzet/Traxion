<?php
/*
* Plugin Name: EcatNext import dico EN
* Plugin URI:
* Description: Import dico from NextPage.
* Version: 0.1
* Slug: ecatnext_dico
* Author: 3C-evolution Jean-Noel Colliard
* Author URI: https://www.3c-e.com
* License: GPLv2 or later
* License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/
?>
<?php
require_once(__DIR__ . '/../../../wp-config.php');
class importDicoEn
{
    private $_files = WP_SITEURL."/wp-content/uploads/wpallimport/files/Export_56.xml";
    function __construct()
    {
        if (isset($_GET["ecatnext_import_dico_en"])) :
            $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
            if ($mysqli->connect_error) :
                die($mysqli->connect_errno);
            endif;
			global $wpdb;
            $truncateDico = "TRUNCATE " . $wpdb->prefix . "ecatnext_dico_en";
            $resultTruncate = $mysqli->query($truncateDico);
            if ($resultTruncate) :
                $results = $this->_start_import();
                $date = new DateTime();
                echo '<pre>' . print_r($date, true) . '</pre>' . "\n" . '<pre>' . print_r($results, true) . '</pre>';
            endif;
        endif;
    }
    private function _start_import()
    {
        set_time_limit(1200);
        $results = [];
        $results = $this->_import_from_xml($this->_files);
        return $results;
    }
    private function _import_from_xml($xml)
    {
        $adds = [];
        $xml = simplexml_load_string(file_get_contents($this->utf8_for_xml($xml)));
        $validedKey = false;
        foreach ($xml->Characteristics as $Characteristics) :
            foreach ($Characteristics->Characteristic as $Characteristic) :
                foreach ($Characteristic->attributes() as $key => $value) :
					//var_dump($key);
                    if ($key == 'ID') {
                        $dicoId = (string)$value;
                    }
                    if (($key == 'CharType') && ($value != '1')) {
                        $validedKey = true;
                    } else {
                        $validedKey = false;
                    }
                    if ($validedKey === true) :
                        foreach ($Characteristic->Translations as $Translations) :
                            foreach ($Translations->Translation as $Translation) :
                                $validedLang = false;
                                foreach ($Translation->attributes() as $key => $value) {
                                    if ($key == 'LangCode' && $value == 'en') {
                                        $validedLang = true;
                                    }
                                }
                                if ($validedLang) :
                                    $labelCat = (string)$Translation->LabelCategorie;
                                    $label = (string)$Translation->Label;
                                    $extId = (string)$Characteristic->ExtID;
                                    if (strpos($labelCat, 'dop') === false) :
                                        if (strpos($labelCat, 'erp') === false) :
                                            if (strpos($labelCat, 'Information logistique') === false) :
                                                if (strpos($labelCat, 'Classification') === false) :
                                                    if (strpos($labelCat, 'SYSTEM') === false) :
                                                        $adds[] = array("DicoId" => $dicoId, "LabelCat" => $labelCat, "Label" => $label, "ExtId" => $extId);
                                                    endif;
                                                endif;
                                            endif;
                                        endif;
                                    endif;
                                endif;
                            endforeach;
                        endforeach;
                    endif;
                endforeach;
            endforeach;
        endforeach;

		var_dump($adds);
        foreach ($adds as $addsSql) :
            $this->_insert_dico($addsSql['DicoId'], $addsSql['LabelCat'], $addsSql['Label'], $addsSql['ExtId']);
        endforeach;
        return $adds;
    }
    private function _insert_dico($DicoId, $LabelCat, $Label, $ExtId)
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        if ($mysqli->connect_error) :
            die($mysqli->connect_errno);
        endif;
		global $wpdb;
        $requestInsert = "INSERT INTO " . $wpdb->prefix . "ecatnext_dico_en (dico_id,dico_name,dico_label,dico_extid) VALUES ('$DicoId','$LabelCat','$Label','$ExtId')";
        $mysqli->query($requestInsert);
        $mysqli->close();
    }
    private function utf8_for_xml($string)
    {
        return preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
    }
}
new importDicoEn();
?>
