<div class="wpallimport-collapsed open pmgi_options">
	<div class="wpallimport-content-section">
		<div class="wpallimport-collapsed-header">
			<h3><?php _e('Gravity Forms Entries','wp_all_import_gf_add_on');?></h3>
		</div>
		<div class="wpallimport-collapsed-content" style="padding: 0;">
			<div class="wpallimport-collapsed-content-inner wpallimport-gf-data">
                <?php if ($form) { echo $form->view(); } ?>
			</div>
		</div>
	</div>
</div>