<div class="wpallimport-collapsed closed">
	<div class="wpallimport-content-section" style="padding-bottom: 0;">
        <div class="wpallimport-collapsed-header" style="margin-bottom: 15px;">
			<h3><?php _e('Entry Notes','wp_all_import_gf_add_on');?></h3>
		</div>
		<div class="wpallimport-collapsed-content" style="padding: 0;">
			<div class="wpallimport-collapsed-content-inner wpallimport-gf-data">
                <table class="form-table" style="max-width:none;">
                    <tr>
                        <td>
                            <table class="form-field wpallimport_variable_table" style="width:98%;">
								<?php
								foreach ($post['pmgi']['notes'] as $i => $note):

									$note += [ 'username' => '', 'date' => 'now', 'note_text' => '', 'note_type' => '', 'note_sub_type' => '' ];

									if (empty($note['note_text'])) continue;

									?>

                                    <tr class="form-field">
                                        <td>
                                            <table style="width:100%;" cellspacing="5">
                                                <tr>
                                                    <td colspan="2">
                                                        <h4><?php _e('Username', 'wp_all_import_gf_add_on'); ?><a href="#help" class="wpallimport-help" style="position:relative; top: -1px; left: 8px;" title="<?php _e('Assign the note to an existing user account by specifying the user ID, username, or e-mail address. Use 0 or leave blank for notification notes.', 'wp_all_import_gf_add_on') ?>">?</a></h4>
                                                        <div class="input">
                                                            <input type="text" name="pmgi[notes][<?php echo $i; ?>][username]" style="width: 100%;font-size: 15px !important;" class="rad4" value="<?php echo esc_attr($note['username']); ?>"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <h4><?php _e('Date Created', 'wp_all_import_gf_add_on') ?><a href="#help" class="wpallimport-help" style="position:relative; top: -1px; left: 8px;" title="<?php _e('Use any format supported by the PHP <b>strtotime</b> function. That means pretty much any human-readable date will work.', 'wp_all_import_gf_add_on') ?>">?</a></h4>
                                                        <div class="input">
                                                            <input type="text" class="datepicker" name="pmgi[notes][<?php echo $i; ?>][date]" value="<?php echo esc_attr($note['date']) ?>"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <h4><?php _e('Note Text', 'wp_all_import_gf_add_on') ?></h4>
                                                        <div class="input">
                                                            <input type="text" name="pmgi[notes][<?php echo $i; ?>][note_text]" value="<?php echo esc_attr($note['note_text']) ?>"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <h4><?php _e('Note Type', 'wp_all_import_gf_add_on'); ?><a href="#help" class="wpallimport-help" style="position:relative; top: -1px; left: 8px;" title="<?php _e('Possible values are \'user\' and \'notification\'.', 'wp_all_import_gf_add_on') ?>">?</a></h4>
                                                        <div class="input">
                                                            <input type="text" name="pmgi[notes][<?php echo $i; ?>][note_type]" value="<?php echo esc_attr($note['note_type']) ?>"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <h4><?php _e('Note Sub-Type', 'wp_all_import_gf_add_on') ?></h4>
                                                        <div class="input">
                                                            <input type="text" name="pmgi[notes][<?php echo $i; ?>][note_sub_type]" value="<?php echo esc_attr($note['note_sub_type']) ?>"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- <hr style="margin: 20px 0;"> -->
                                        </td>
                                        <td class="action remove"><!--a href="#remove" style="top: 5px;"></a--></td>
                                    </tr>

								<?php endforeach; ?>
                                <tr class="form-field template">
                                    <td>
                                        <table style="width:100%;" cellspacing="5">
                                            <tr>
                                                <td colspan="2">
                                                    <h4><?php _e('Username', 'wp_all_import_gf_add_on'); ?><a href="#help" class="wpallimport-help" style="position:relative; top: -1px; left: 8px;" title="<?php _e('Assign the note to an existing user account by specifying the user ID, username, or e-mail address. Use 0 or leave blank for notification notes.', 'wp_all_import_gf_add_on') ?>">?</a></h4>
                                                    <div class="input">
                                                        <input type="text" name="pmgi[notes][ROWNUMBER][username]" style="width: 100%;" class="rad4"/>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <h4><?php _e('Date Created', 'wp_all_import_gf_add_on'); ?><a href="#help" class="wpallimport-help" style="position:relative; top: -1px; left: 8px;" title="<?php _e('Use any format supported by the PHP <b>strtotime</b> function. That means pretty much any human-readable date will work.', 'wp_all_import_gf_add_on') ?>">?</a></h4>
                                                    <div class="input">
                                                        <input type="text" name="pmgi[notes][ROWNUMBER][date]" class="date-picker" value="now"/>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <h4><?php _e('Note Text', 'wp_all_import_gf_add_on'); ?></h4>
                                                    <div class="input">
                                                        <input type="text" name="pmgi[notes][ROWNUMBER][note_text]" style="width: 100%;" class="rad4"/>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <h4><?php _e('Note Type', 'wp_all_import_gf_add_on'); ?><a href="#help" class="wpallimport-help" style="position:relative; top: -1px; left: 8px;" title="<?php _e('Possible values are \'user\' and \'notification\'.', 'wp_all_import_gf_add_on') ?>">?</a></h4>
                                                    <div class="input">
                                                        <input type="text" name="pmgi[notes][ROWNUMBER][note_type]" style="width: 100%;" class="rad4"/>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <h4><?php _e('Note Sub-Type', 'wp_all_import_gf_add_on'); ?></h4>
                                                    <div class="input">
                                                        <input type="text" name="pmgi[notes][ROWNUMBER][note_sub_type]" style="width: 100%;" class="rad4"/>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="action remove"></td>
                                </tr>
                                <tr class="wpallimport-row-actions" style="display:none;">
                                    <td colspan="2">
                                        <a class="add-new-line" title="Add Another Note" href="javascript:void(0);" style="width:200px;"><?php _e("Add Another Note", 'wp_all_import_gf_add_on'); ?></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
			</div>

            <div class="wpallimport-collapsed closed wpallimport-section">
                <div style="margin:0; border-top:1px solid #ddd; border-bottom: none; border-right: none; border-left: none; background: #f1f2f2;" class="wpallimport-content-section rad0">
                    <div class="wpallimport-collapsed-header">
                        <h3 style="color:#40acad;"><?php _e("Advanced Options", 'wp_all_import_gf_add_on');?></h3>
                    </div>
                    <div style="padding: 0px;" class="wpallimport-collapsed-content">
                        <div class="wpallimport-collapsed-content-inner">
							<?php if ( empty(PMXI_Plugin::$session->options['delimiter']) ): ?>
                                <div class="form-field wpallimport-radio-field wpallimport-clear pmxi_option">
                                    <input type="radio" id="notes_repeater_mode_variable_csv" name="pmgi[notes_repeater_mode]" value="csv" <?php echo 'csv' == $post['pmgi']['notes_repeater_mode'] ? 'checked="checked"' : '' ?> class="switcher variable_repeater_mode"/>
                                    <label for="notes_repeater_mode_variable_csv" style="width:auto;"><?php _e('Fixed Repeater Mode', 'wp_all_import_gf_add_on') ?></label>
                                    <div class="switcher-target-notes_repeater_mode_variable_csv wpallimport-clear" style="padding: 10px 0 10px 25px; overflow: hidden;">
									<span class="order-separator-label wpallimport-slide-content" style="padding-left:0;">
										<label><?php _e('Multiple notes separated by', 'wp_all_import_gf_add_on'); ?></label>
										<input type="text" class="short rad4 order-separator-input" name="pmgi[notes_repeater_mode_separator]" value="<?php echo esc_attr($post['pmgi']['notes_repeater_mode_separator']) ?>" style="width:10%; text-align: center;"/>
										<a href="#help" class="wpallimport-help" style="top:0px;left:8px;" title="For example, two notes would be imported like this Note 1|Note 2">?</a>
									</span>
                                    </div>
                                </div>
                                <div class="form-field wpallimport-radio-field wpallimport-clear pmxi_option">
                                    <input type="radio" id="notes_repeater_mode_variable_xml" name="pmgi[notes_repeater_mode]" value="xml" <?php echo 'xml' == $post['pmgi']['notes_repeater_mode'] ? 'checked="checked"' : '' ?> class="switcher variable_repeater_mode"/>
                                    <label for="notes_repeater_mode_variable_xml" style="width:auto;"><?php _e('Variable Repeater Mode', 'wp_all_import_gf_add_on') ?></label>
                                    <div class="switcher-target-notes_repeater_mode_variable_xml wpallimport-clear" style="padding: 10px 0 10px 25px; overflow: hidden;">
									<span class="wpallimport-slide-content" style="padding-left:0;">
										<label style="width: 60px;"><?php _e('For each', 'wp_all_import_gf_add_on'); ?></label>
										<input type="text" class="short rad4" name="pmgi[notes_repeater_mode_foreach]" value="<?php echo esc_attr($post['pmgi']['notes_repeater_mode_foreach']) ?>" style="width:50%;"/>
										<label style="padding-left: 10px;"><?php _e('do...', 'wp_all_import_gf_add_on'); ?></label>
									</span>
                                    </div>
                                </div>
							<?php else: ?>
                                <input type="hidden" name="pmgi[notes_repeater_mode]" value="csv"/>
                                <div class="form-field input" style="padding-left: 20px;">
                                    <label class="order-separator-label" style="line-height: 30px;"><?php _e('Multiple notes separated by', 'wp_all_import_gf_add_on'); ?></label>
                                    <input type="text" class="short rad4 order-separator-input" name="pmgi[notes_repeater_mode_separator]" value="<?php echo esc_attr($post['pmgi']['notes_repeater_mode_separator']) ?>" style="width:10%; text-align: center;"/>
                                    <a href="#help" class="wpallimport-help" style="top:0px;left:8px;" title="For example, two notes would be imported like this 'Note 1|Note 2'">?</a>
                                </div>
							<?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>