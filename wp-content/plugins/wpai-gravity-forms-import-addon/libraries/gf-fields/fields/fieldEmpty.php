<?php

namespace wpai_gravityforms_add_on\gf\fields;

/**
 * Class FieldEmpty.
 *
 * @package wpai_gravityforms_add_on\gf\fields
 */
class FieldEmpty extends Field {

    /**
     *  Field type key
     */
    public $type = 'empty';

	/**
	 * @return mixed|void
	 */
	public function view() {}

}