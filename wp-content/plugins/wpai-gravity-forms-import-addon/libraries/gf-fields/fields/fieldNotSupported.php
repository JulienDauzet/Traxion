<?php

namespace wpai_gravityforms_add_on\gf\fields;

/**
 * Class FieldEmpty
 * @package wpai_gravityforms_add_on\gf\fields
 */
class FieldNotSupported extends Field {

    /**
     *  Field type key
     */
    public $type = 'not_supported';



}