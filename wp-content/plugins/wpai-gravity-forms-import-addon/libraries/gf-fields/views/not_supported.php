<p>
    <?php
    _e('This field type is not supported. E-mail support@wpallimport.com with the details of the custom Gravity Forms field you are trying to import to, as well as a link to download the plugin to install to add this field type to Gravity Forms, and we will investigate the possibility ot including support for it in the Gravity Forms add-on.', 'wp_all_import_gf_add_on');
    ?>
</p>