��          �                       -     6     F     ^     q  #   �  ,   �     �     �               -     H  i   c  �  �     �     �     �     �     �  $     $   8  '   ]     �     �     �     �     �       B   !   Default sorting Products Search products Search products&hellip; Search products… Search results for "%s" Search results for &ldquo;%s&rdquo; Showing all %d result Showing all %d results Showing the single result Sort by average rating Sort by latest Sort by popularity Sort by price: high to low Sort by price: low to high with first and last resultShowing %1$d&ndash;%2$d of %3$d result Showing %1$d&ndash;%2$d of %3$d results Project-Id-Version: WooCommerce 5.8.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/woocommerce
Last-Translator: 
Language-Team: Français
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2021-10-12T16:27:50+00:00
PO-Revision-Date: 2021-11-12 11:30+0000
X-Generator: Loco https://localise.biz/
X-Domain: woocommerce
Language: fr_FR
Plural-Forms: nplurals=2; plural=n > 1;
X-Loco-Version: 2.5.5; wp-5.8.1 Trier par défaut Produits Rechercher des produits Rechercher des produits... Rechercher des produits... Résultats de la recherche pour "%s" Résultats de la recherche pour "%s" %d produit trouvé %d produits trouvés Un seul produit trouvé Trier par note moyenne Trier par date de mise en ligne Trier par popularité Trier par prix: Décroissant Trier par prix: Croissant %1$d&ndash;%2$d sur %3$d produit %1$d&ndash;%2$d sur %3$d produits 