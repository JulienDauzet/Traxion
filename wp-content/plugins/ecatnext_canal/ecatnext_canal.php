<?php

/*

* Plugin Name: EcatNext import canal

* Plugin URI:

* Description: Import canal web from NextPage.

* Version: 0.1

* Slug: ecatnext_canal

* Author: 3C-evolution Jean-Noel Colliard

* Author URI: https://www.3c-e.com

* License: GPLv2 or later

* License URI: http://www.gnu.org/licenses/gpl-2.0.html

*/

?>

<?php

// Nous appelons le fichier par défaut de WordPress des configurations pour notre import.

require_once(__DIR__ . '/../../../wp-config.php');

class importCanal

{

    // On récupère le fichier d'export Canal de NextPage.

    // variable $_files modifier bien l'url d'appel.
    private $_files = WP_SITEURL."/wp-content/uploads/wpallimport/files/ecatnext_canal.xml";

    function __construct()

    {

        if (isset($_GET["ecatnext_import_canal"])) :

            $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

            if ($mysqli->connect_error) :
                die($mysqli->connect_errno);

            endif;

			

			global $wpdb;

            // On vide la table ecatnext_canal avant de relancer l'import.

            $truncateCanal = "TRUNCATE " . $wpdb->prefix . "ecatnext_canal";

            $resultTruncate = $mysqli->query($truncateCanal);

            if ($resultTruncate) :

                // Nous allons utiliser la function _start_import().

                $results = $this->_start_import();

				

                $date = new DateTime();

                // On affiche coté client les informations pour vérification de réception de données.

                //echo '<pre>' . print_r($date, true) . '</pre>' . "\n" . '<pre>' . print_r($results, true) . '</pre>';

            endif;

        endif;

    }

    private function _start_import()

    {

        set_time_limit(1200);

        $results = [];

        // Nous allons utiliser la function _import_from_xml() pour l'import.

        $results = $this->_import_from_xml($this->_files);

        return $results;

    }

    private function _import_from_xml($xml)

    {

		

        $adds = [];

        // Nous utilisons la function PHP simplexml_load_string et notre function utf8_for_xml().

        $xml = simplexml_load_string(file_get_contents($this->utf8_for_xml($xml)));

        $validedKey = false;

        // Pour parser le contenu du fichier XML nous allons faire des boucles sur les différents noeuds.

				//var_dump($xml->Elements);

        foreach ($xml->Elements as $Elements) :

            foreach ($Elements->Element as $Element) :

                
        
        
                foreach ($Element->attributes() as $key => $value) :

				

                    if ($key == 'ID') {

                        $canalId = (int)$value;

                    }

                    if ($key == 'ParentID') {

                        $canalParendId = (int)$value;

                    }

                    if ($key == 'Order') {

                        $canalOrder = (int)$value;

                    }

                    if (!empty($canalId) && !empty($canalOrder) && !empty($canalParendId)) :

                        $validedKey = true;

                    endif;


                    //var_dump($validedKey);

                endforeach;

				//var_dump($validedKey);

                if ($validedKey === true) :

                    foreach ($Element->Translations as $Translations) :

                        $Label = (string)$Translations->Label;

                    endforeach;

                endif;

                if ($Element->Characteristics == true) :

                    foreach ($Element->Characteristics as $Characteristics) :

                        foreach ($Characteristics->Characteristic as $Characteristic) :

                            foreach ($Characteristic->Values as $Values) :

                                foreach ($Values->Value as $Value) :									

										//$productId = (int)$Value->Links->LinkedItem->attributes('');
                                        if ($Value->Links->LinkedItem != null) :

                                                echo "<pre>";
                                                var_dump($Value->Links->LinkedItem->attributes()->ID);
                                                echo "</pre>";
                                                $productId = $Value->Links->LinkedItem->attributes('');
                                                

                                        endif;

                                endforeach;

                            endforeach;

                        endforeach;

                    endforeach;

                else :

                    $productId = '';

                endif;

                $adds[] = array("Id" => $canalId, "ParentId" => $canalParendId, "Order" => $canalOrder, "Label" => $Label, "ProductId" => $productId);

                //echo '<pre>';
                //var_dump($adds);
                //echo '</pre>';
            endforeach;

        endforeach;

		

		//var_dump($adds);

        foreach ($adds as $addsSql) :

            // Une fois que nous avons associer toutes nos valeurs dans notre tableau $adds, noud allons utiliser la function _insert_canal pour les insérer dans notre table.

           // $this->_insert_canal($addsSql['Id'], $addsSql['ParentId'], $addsSql['Order'], utf8_decode(addslashes($addsSql['Label'])), $addsSql['ProductId']);
            $this->_insert_canal($addsSql['Id'], $addsSql['ParentId'], $addsSql['Order'], addslashes($addsSql['Label']), $addsSql['ProductId']);

        endforeach;

        return $adds;

    }

    private function _insert_canal($Id, $ParentId, $Order, $Label, $ProductId)

    {

        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

        if ($mysqli->connect_error) :

            die($mysqli->connect_errno);

        endif;

		

			global $wpdb;

        // Nous allons vérifier si le canal fait partie du menu principale ou secondaire pour insérer une valeur par défaut, dans notre cas 0.

        if (!empty($ProductId)) :

            $requestInsert = "INSERT INTO " . $wpdb->prefix . "ecatnext_canal (canal_id,canal_parentid,canal_order,canal_label,canal_productid) VALUES ('$Id','$ParentId','$Order','$Label','$ProductId')";

        else :

            $requestInsert = "INSERT INTO " . $wpdb->prefix . "ecatnext_canal (canal_id,canal_parentid,canal_order,canal_label,canal_productid) VALUES ('$Id','$ParentId','$Order','$Label','0')";

        endif;

		

		

		

        $mysqli->query($requestInsert);

        $mysqli->close();

    }

    private function utf8_for_xml($string)

    {

        return preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);

    }

}

new importCanal();

?>