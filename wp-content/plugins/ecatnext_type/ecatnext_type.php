<?php
/*
* Plugin Name: EcatNext import type
* Plugin URI:
* Description: Import dico from NextPage.
* Version: 0.1
* Slug: ecatnext_type
* Author: 3C-evolution Jean-Noel Colliard
* Author URI: https://www.3c-e.com
* License: GPLv2 or later
* License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/
?>
<?php
require_once(__DIR__ . '/../../../wp-config.php');
class importType
{
    private $_files = WP_SITEURL."/wp-content/uploads/wpallimport/files/ecatnext_type_utf8.csv";

    function __construct()
    {
        if (isset($_GET["ecatnext_import_type"])) :
            $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
            if ($mysqli->connect_error) :
                die($mysqli->connect_errno);
            endif;
			global $wpdb;
            $truncateType = "TRUNCATE " . $wpdb->prefix . "ecatnext_type";
            $resultTruncate = $mysqli->query($truncateType);
            if ($resultTruncate) :
                $results = $this->_start_import();
                $date = new DateTime();
                echo '<pre>' . print_r($date, true) . '</pre>' . "\n" . '<pre>' . print_r($results, true) . '</pre>';
            endif;
        endif;
    }
    private function _start_import()
    {
        set_time_limit(1200);
        $results = [];
        $results = $this->_import_from_csv($this->_files);
        return $results;
    }
    private function _import_from_csv($csv)
    {
        $adds = [];
        $csvtemp = [];
        $csvfile = file_get_contents($csv);
        $csv = str_getcsv($csvfile, "\n");
        foreach ($csv as $key => $item) :
            array_push($csvtemp, str_getcsv($item, ";"));
        endforeach;
        foreach ($csvtemp as $key => $item) :
            if ($item[11] != '' && $item[13] != '') :
                array_push($adds, $item);
            endif;
        endforeach;
		
        foreach ($adds as $addsSql) :
            $this->_instal_type(utf8_encode($addsSql[11]));
        endforeach;
        //return $adds;
    }
    private function _instal_type($ValueType)
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        if ($mysqli->connect_error) :
            die($mysqli->connect_errno);
        endif;
		global $wpdb;
        $ValueType = str_replace(' ', '', $ValueType);
        $requestSelect = "SELECT * FROM " . $wpdb->prefix . "ecatnext_type WHERE type_value='$ValueType'";
        $requestReturn = $mysqli->query($requestSelect);
        $requestCount = mysqli_num_rows($requestReturn);
        if ($requestCount == 0) :
            $requestInsert = "INSERT INTO " . $wpdb->prefix . "ecatnext_type (type_value) VALUES ('$ValueType')";
            $mysqli->query($requestInsert);
            $mysqli->close();
        endif;
    }
}
new importType();
?>