<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'traxion2' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/** utilisé pour le connecteur ecatnext */
define( 'WP_SITEURL', 'http://localhost/traxion' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';ZrDVG)^eIb>@Dl4h5;50%JVzs]MzXXXpQSdLysp;fquOokRgC.EDM_lhu`!q)Mf' );
define( 'SECURE_AUTH_KEY',  'O3z#0$Zrt0ElibnLK`PkdcOkD62<gtm9;!/o3`pd%.5l04A|j>HF>L8%T; kxKPX' );
define( 'LOGGED_IN_KEY',    'J<i<R$@@OSAt6u9.Yhdmv:gtT80~ThWD5]k1z9G({:vtQYZMi:#g,k]d0Jr9e.7?' );
define( 'NONCE_KEY',        't6r_E%+yd5zbCqcwR]K,?aeLio:8@5y]!n[>1Ikn,`yeEPnk:!7R y.LT~lAS^G7' );
define( 'AUTH_SALT',        'P%#gC31u6Qu I)I.!;Rt<l]4y|%+)RLCeSY_/F0qkV/b!U*)kJz^?0Vl;s8jGG,{' );
define( 'SECURE_AUTH_SALT', '?aeAufVlV2^B=t[8B&J+qhcGcPntKZYk6KY|!jX[MA|FV#iTC.4YCyoMdl$OW#2s' );
define( 'LOGGED_IN_SALT',   '6(.A?)N,0#5j[9SvF1=jau+diFzT8]XjK1xlx*ks;}jlB:0hxW;2rU93Vm@x(C2p' );
define( 'NONCE_SALT',       '`UIg,lijLI,I%-B?TCiCD}yjd*8-v#~_{K@WJws_,O5l+k6y!BtF<cxvqXpXzI9P' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
